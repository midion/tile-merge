Tile Merge Game
===============

A fun game where the objective is to combine tiles with same number in order to
get a tile with the biggest number possible.

## Quick Start

### Generate and upload your SSH key

```shell
ssh-keygen -t rsa -C "youremail@yourprovider"
```

- Copy the content of the line ending with your email in `~/.ssh/id_rsa.pub`
- At the top right corner in GitLab, click your photo/icon and then click on Profile Settings
- Click on SSH Keys tab
- Paste the key and click on Add key

### Configure git to use your email

```shell
git config --global user.name "Your Name"
git config --global user.email "youremail@yourprovider"
```


### Clone the repository

```shell
git clone git@gitlab.com:midion/tile-merge.git
```

## Collaborating to the project

### Branch off master

```shell
git checkout master
git pull origin
git checkout -b my-feature
```

### Commit your changes to your local branch

After ready with some changes:

```shell
git status
git add somefile1 somefile2
git commit -m "Some message"
```

### Push your local branch to the repository

After ready to share your code with the world:

```shell
git push origin my-feature
```

### Raise a pull request

When ready to ask to incorporate your code into the official one, raise a merge request.

But first, you'll have to merge any conflicts if there's any. The command `git status` will show you if someone has modified the same files as you.

```shell
git pull origin master
git status
git push origin my-feature
```
