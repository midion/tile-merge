import GameController from "./game-controller";
import InputController from "./input-controller";
import StorageController from "./storage-controller";
import ViewController from "./view-controller";

screen.lockOrientation && screen.lockOrientation("portrait");

function boot() {
    const ready = document.readyState === "complete";
    ready && window.requestAnimationFrame(function () {
        const size = window.SIZE || 3;

        const gridContainer = document.querySelector(".grid-container");
        for (let y = 0; y < size; y++) {
            for (let x = 0; x < size; x++) {
                const gridCell = document.createElement("div");
                gridCell.className = "grid-cell";
                gridContainer.appendChild(gridCell);
            }
        }
        for (let i = 0; i < gridContainer.classList.length; i++) {
            const className = gridContainer.classList[i];
            if (/^[.]grid-container-[0-9]+$/.test(className)) {
                gridContainer.classList.remove(className);
            }
        }
        gridContainer.classList.add(`size-${size}`);

        const tileContainer = document.querySelector(".tile-container");
        for (let i = 0; i < gridContainer.classList.length; i++) {
            const className = tileContainer.classList[i];
            if (/^[.]tile-container-[0-9]+$/.test(className)) {
                tileContainer.classList.remove(className);
            }
        }
        tileContainer.classList.add(`size-${size}`);

        const container = document.querySelector(".container");
        new GameController(
            size,
            new InputController(
                document.querySelector(".game-container"),
                document.querySelector(".restart-button")
            ),
            new StorageController(size),
            new ViewController(size, container)
        );

        container.style.visibility = "visible";
    });
    return ready;
}
boot() || document.addEventListener("readystatechange", boot);
