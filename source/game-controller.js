import { Grid, Tile } from "./model";

const MAP = [
    { x: 0, y: -1 },
    { x: 1, y: 0 },
    { x: 0, y: 1 },
    { x: -1, y: 0 }
];

class GameController {
    constructor(size, inputCtrl, storageCtrl, viewCtrl) {
        this.size = size;
        this.inputCtrl = inputCtrl;
        this.storageCtrl = storageCtrl;
        this.viewCtrl = viewCtrl;

        this.startTiles = 2;

        this.inputCtrl.on("move", direction => this.move(direction));
        this.inputCtrl.on("restart", () => this.restart());
        this.inputCtrl.on("keepPlaying", () => this.keepPlaying());

        this.setup();
    }
    get terminated() {
        return this.over || (this.won && !this.keepPlaying);
    }
    restart() {
        this.storageCtrl.gameState = null;
        this.viewCtrl.continueGame();
        this.setup();
    }
    keepPlaying() {
        this.keepPlaying = true;
        this.viewCtrl.continueGame();
    }
    setup() {
        const previousState = this.storageCtrl.gameState;
        if (previousState) {
            this.grid = new Grid(previousState.grid.size, previousState.grid.cells);
            this.score = previousState.score;
            this.over = previousState.over;
            this.won = previousState.won;
            this.keepPlaying = previousState.keepPlaying;
        } else {
            this.grid = new Grid(this.size);
            this.score = 0;
            this.over = false;
            this.won = false;
            this.keepPlaying = false;
            this.addStartTiles();
        }
        this.updateView();
    }
    addStartTiles() {
        for (let i = 0; i < this.startTiles; i++) {
            this.addRandomTile();
        }
    }
    addRandomTile() {
        if (this.grid.availableCells.length) {
            const value = Math.random() < 0.9 ? 2 : 4;
            const tile = new Tile(this.grid.randomAvailableCell(), value);
            this.grid.insertTile(tile);
        }
    }
    updateView() {
        this.storageCtrl.bestScore = Math.max(this.storageCtrl.bestScore, this.score);
        this.storageCtrl.gameState = this.over ? null : this.serialize();

        this.viewCtrl.update(this.grid, {
            score: this.score,
            over: this.over,
            won: this.won,
            bestScore: this.storageCtrl.bestScore,
            terminated: this.terminated
        });
    }
    serialize() {
        return {
            grid: this.grid.serialize(),
            score: this.score,
            over: this.over,
            won: this.won,
            keepPlaying: this.keepPlaying
        };
    }
    moveTile(tile, cell) {
        this.grid.cells[tile.x][tile.y] = null;
        this.grid.cells[cell.x][cell.y] = tile;
        tile.updatePosition(cell);
    }
    prepareTiles() {
        for (let x = 0; x < this.grid.size; x++) {
            for (let y = 0; y < this.grid.size; y++) {
                const tile = this.grid.cells[x][y];
                if (tile) {
                    tile.mergedFrom = tile.mergedTo = null;
                    tile.savePosition();
                }
            }
        }
    }
    move(direction) {
        if (this.terminated) {
            return;
        }

        let vector = this.getVector(direction);
        let traversals = this.buildTraversals(vector);
        let moved = false;

        this.prepareTiles();

        for (const x of traversals.x) {
            for (const y of traversals.y) {
                const cell = { x: x, y: y };
                const tile = this.grid.getCellContent(cell);
                if (tile) {
                    const positions = this.findFarthestPosition(cell, vector);
                    const next = this.grid.getCellContent(positions.next);
                    if (next && next.value === tile.value && !next.mergedFrom) {
                        const merged = new Tile(positions.next, tile.value << 1);
                        tile.mergedTo = next.mergedTo = merged;
                        merged.mergedFrom = [tile, next];
                        this.grid.insertTile(merged);
                        this.grid.removeTile(tile);
                        tile.updatePosition(positions.next);
                        this.score += merged.value;
                        if (merged.value === (1 << (this.size * this.size))) {
                            this.won = true;
                        }
                    } else {
                        this.moveTile(tile, positions.farthest);
                    }
                    if (!this.positionsEqual(cell, tile)) {
                        moved = true;
                    }
                }
            }
        }

        if (moved) {
            this.addRandomTile();
            if (!this.movesAvailable) {
                this.over = true;
            }
            this.updateView();
        }
    }
    getVector(direction) {
        return MAP[direction];
    }
    buildTraversals(vector) {
        const traversals = { x: [], y: [] };

        for (let pos = 0; pos < this.size; pos++) {
            traversals.x.push(pos);
            traversals.y.push(pos);
        }

        if (vector.x === 1) {
            traversals.x = traversals.x.reverse();
        }
        if (vector.y === 1) {
            traversals.y = traversals.y.reverse();
        }

        return traversals;
    }
    findFarthestPosition(cell, vector) {
        let farthest, next = cell;

        do {
            farthest = next;
            next = { x: farthest.x + vector.x, y: farthest.y + vector.y };
        } while (this.grid.isWithinBounds(next) && this.grid.isCellAvailable(next));

        return { farthest, next };
    }
    get movesAvailable() {
        return this.grid.availableCells.length || this.tileMatchesAvailable;
    }
    get tileMatchesAvailable() {
        for (let x = 0; x < this.size; x++) {
            for (let y = 0; y < this.size; y++) {
                const tile = this.grid.getCellContent({ x: x, y: y });
                if (tile) {
                    for (let direction = 0; direction < 4; direction++) {
                        const vector = this.getVector(direction);
                        const cell = { x: x + vector.x, y: y + vector.y };
                        const other  = this.grid.getCellContent(cell);
                        if (other && other.value === tile.value) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    positionsEqual(first, second) {
        return first.x === second.x && first.y === second.y;
    }
}

export default GameController;
