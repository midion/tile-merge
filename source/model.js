export class Tile {
    constructor(position, value) {
        this.x = position.x;
        this.y = position.y;
        this.value = value || 2;
        this.previousPosition = null;
        this.mergedFrom = null;
    }
    savePosition() {
        this.previousPosition = { x: this.x, y: this.y };
    }
    updatePosition(position) {
        this.x = position.x;
        this.y = position.y;
    }
    serialize() {
        const position = { x: this.x, y: this.y };
        const value = this.value;
        return { position, value };
    }
}

export class Grid {
    constructor(size, previousState) {
        this.size = size;
        this.cells = previousState ? this.fromState(previousState) : this.empty();
    }
    empty() {
        const cells = [ ];
        for (let x = 0; x < this.size; x++) {
            cells[x] = [ ];
            for (let y = 0; y < this.size; y++) {
                cells[x].push(null);
            }
        }
        return cells;
    }
    fromState(state) {
        const cells = [ ];
        for (let x = 0; x < this.size; x++) {
            cells[x] = [ ];
            for (let y = 0; y < this.size; y++) {
                cells[x].push(state[x][y] ? new Tile(state[x][y].position, state[x][y].value) : null);
            }
        }
        return cells;
    }
    randomAvailableCell() {
        const cells = this.availableCells;
        return cells.length ? cells[Math.floor(Math.random() * cells.length)] : null;
    }
    get availableCells() {
        const cells = [ ];

        for (let x = 0; x < this.size; x++) {
            for (let y = 0; y < this.size; y++) {
                !this.cells[x][y] && cells.push({ x: x, y: y });
            }
        }
        return cells;
    }
    isWithinBounds(position) {
        const xIsGood = position.x >= 0 && position.x < this.size;
        const yIsGood = position.y >= 0 && position.y < this.size;
        return xIsGood && yIsGood;
    }
    insertTile(tile) {
        this.cells[tile.x][tile.y] = tile;
    }
    removeTile(tile) {
        this.cells[tile.x][tile.y] = null;
    }
    serialize() {
        const cells = [ ];
        const size = this.size;
        for (let x = 0; x < size; x++) {
            cells[x] = [ ];
            for (let y = 0; y < size; y++) {
                cells[x].push(this.cells[x][y] ? this.cells[x][y].serialize() : null);
            }
        }
        return { size, cells };
    }
    isCellAvailable(cell) {
        return !this.isCellOccupied(cell);
    }
    isCellOccupied(cell) {
        return !!this.getCellContent(cell);
    }
    getCellContent(cell) {
        return this.isWithinBounds(cell) ? this.cells[cell.x][cell.y] : null;
    }
}
