
const messages = {
    gameOver: "Game Over!",
    gameWon: "Brillant! You Won!"
}

class ViewController {
    constructor(gridSize, container) {
        this._container = container;

        this._headingContainer = container.querySelector(".heading")
        this._aboveGameContainer = container.querySelector(".above-game");
        this._gameContainer = container.querySelector(".game-container");
        this._gridContainer = container.querySelector(".grid-container");
        this._tileContainer = container.querySelector(".tile-container");
        this._scoreContainer = container.querySelector(".score-container");
        this._bestContainer = container.querySelector(".best-container");
        this._messageElement = container.querySelector(".game-message");

        this._gridSize = gridSize;
        this._score = 0;
        this._bestScore = 0;

        const calcWidthProps = () => {

            const computedStyle = window.getComputedStyle(this._container);
            const paddingWidth = parseFloat(computedStyle.paddingLeft) + parseFloat(computedStyle.paddingRight);
            const paddingHeight = parseFloat(computedStyle.paddingTop) + parseFloat(computedStyle.paddingBottom);
            const borderWidth = parseFloat(computedStyle.borderLeftWidth) + parseFloat(computedStyle.borderRightWidth);
            const borderHeight = parseFloat(computedStyle.borderTopWidth) + parseFloat(computedStyle.borderBottomWidth);
            const width = this._container.offsetWidth;
            const height = this._container.offsetHeight * 0.72;

            const availableWidth = Math.min(width - paddingWidth - borderWidth, height - paddingHeight - borderHeight);

            this._gridWidth = availableWidth;
            this._tileWidth = availableWidth * 0.92 / this._gridSize;
            this._tileHeight = this._tileWidth;
            this._spacingWidth = availableWidth * 0.08 / (this._gridSize + 1);
            this._spacingHeight = this._spacingWidth;

            const gridWidthPx = `${this._gridWidth}px`;
            this._gameContainer.style.width = gridWidthPx;
            this._gameContainer.style.height = gridWidthPx;
            this._gameContainer.style["font-size"] = `${this._tileWidth / 8}px`;
            this._aboveGameContainer.style.width = gridWidthPx;
            this._aboveGameContainer.style["font-size"] = `${this._gridWidth / 30}px`;

            this._headingContainer.style["font-size"] = `${this._gridWidth / 40}px`;

            for (let i = 0; i < this._tileContainer.childNodes.length; i++) {
                const tileElem = this._tileContainer.childNodes[i];
                this._updateTilePosition(tileElem, {
                    x: parseInt(tileElem.getAttribute("x"), 10),
                    y: parseInt(tileElem.getAttribute("y"), 10)
                });
            }
        };
        calcWidthProps();

        window.addEventListener("resize", () => {
            calcWidthProps();
        });
    }
    update(grid, data) {
        window.requestAnimationFrame(() => {
            this._tileContainer.innerHTML = "";

            for (const column of grid.cells) {
                for (const cell of column) {
                    cell && this.addTile(cell);
                }
            }

            if (!this._bestScore && data.bestScore) {
                this._bestScore = data.bestScore;
            }
            this._updateScore(data.score, data.bestScore);

            if (data.terminated) {
                if (data.over) {
                    this._messageElement.classList.add("game-over");
                    this._messageElement.innerHTML = `<span>${messages.gameOver}</span>`;
                } else if (data.won) {
                    this._messageElement.classList.add("game-won");
                    this._messageElement.innerHTML = `<span>${messages.gameWon}</span>`;
                }
            }
        });
    }
    continueGame() {
        for (let i = 0; i < this._messageElement.classList.length; i++) {
            const className = this._messageElement.classList[i];
            if (/^game-(won|over)/.test(className)) {
                this._messageElement.classList.remove(className);
            }
        }
        this._messageElement.innerHTML = "";
    }
    _updateTilePosition(tileElem, position) {
        const x = Math.round(this._tileWidth * position.x + this._spacingWidth * (position.x + 1));
        const y = Math.round(this._tileHeight * position.y + this._spacingHeight * (position.y + 1));

        // use CPU
        //tileElem.style.left = `${x}px`;
        //tileElem.style.top = `${y}px`;
        // use GPU
        const transform = `translate(${x}px, ${y}px)`;
        tileElem.style.transform = transform;
        tileElem.style["-webkit-transform"] = transform;
        tileElem.style["-moz-transform"] = transform;
        tileElem.style["-ms-transform"] = transform;

        tileElem.setAttribute("x", position.x);
        tileElem.setAttribute("y", position.y);
    }
    addTile(tile) {
        const tileWrapElem = document.createElement("div");
        const tileElem = document.createElement("div");
        const innerElem = document.createElement("div");
        const position = { x: tile.x, y: tile.y };

        tileWrapElem.className = "tile-wrap";
        tileElem.className = `tile tile-${tile.value}`;

        this._updateTilePosition(tileWrapElem, tile.previousPosition || position);

        innerElem.classList.add("tile-inner");
        innerElem.innerHTML = tile.value;

        if (tile.previousPosition) {
            if (tile.mergedTo) {
               tileElem.classList.add("tile-merging");
            }
            window.requestAnimationFrame(() => {
                this._updateTilePosition(tileWrapElem, position);
            });
        } else if (tile.mergedFrom) {
            tileElem.classList.add("tile-merged");
            for (const merged of tile.mergedFrom) {
                this.addTile(merged);
            }
        } else {
            tileElem.classList.add("tile-new");
        }

        tileElem.appendChild(innerElem);
        tileWrapElem.appendChild(tileElem);
        this._tileContainer.appendChild(tileWrapElem);
    }
    _updateScore(score, bestScore) {
        const scoreDiff = score - this._score;
        this._score = score;
        if (scoreDiff > 0) {
            this._scoreContainer.innerHTML = `<span class="updated">${this._score}</span>`;
        } else {
            this._scoreContainer.innerHTML = `<span>${this._score}</span>`;
        }

        const bestScoreDiff = bestScore - this._bestScore;
        this._bestScore = bestScore;

        if (bestScoreDiff > 0) {
            this._bestContainer.innerHTML = `<span class="updated">${this._bestScore}</span>`;
        } else {
            this._bestContainer.innerHTML = `<span>${this._bestScore}</span>`;
        }
    }
}

export default ViewController;
